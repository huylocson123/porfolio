import "./App.css";
import Home from "./HomeTemplate/Home";

function App() {
  return (
    <main className="text-gray-400 bg-gray-900 body-font">
      <Home />
    </main>
  );
}

export default App;
