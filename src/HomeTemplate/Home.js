import React from "react";
import About from "../Component/About";
import Canvas from "../Component/Canvas";
import Contact from "../Component/Contact";
import NavBar from "../Component/NavBar";
import Project from "../Component/Project";
import Skill from "../Component/Skill";
import Testimonials from "../Component/Testimonials";

export default function Home() {
  return (
    <div>
      {/* <Canvas /> */}
      <div className="relative">
        <NavBar />
        <About />
        <Project />
        <Skill />
        <Testimonials />
        <Contact />
      </div>
    </div>
  );
}
